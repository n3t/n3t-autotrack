<?php
/**
 * @package n3t Autotrack
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2018-2019 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

class plgSystemN3tAutotrack extends JPlugin {

  protected function formatOptionValue($value, $valueType = null, $keys = [])
  {
    switch ($valueType) {
      case 'array':
        $newValue = [];
        foreach($value as $objValue)
          if (is_object($objValue) && @$objValue->value)
            $newValue[] = $objValue->value;
          elseif ($objValue)
            $newValue[] = $objValue;
        $value = $newValue;
        break;

      case 'object':
        $newValue = [];
        foreach($value as $objValue)
          if (is_object($objValue) && @$objValue->name && property_exists($objValue, 'value'))
            $newValue[$objValue->name] = $objValue->value;
          elseif (is_array($objValue) && @$objValue['name'] && array_key_exists($objValue, 'value'))
            $newValue[$objValue['name']] = $objValue['value'];
        $value = $newValue;
        break;

      case 'arrayobject':
        $newValue = [];
        foreach($value as $objValue) {
          $oneValue = [];
          foreach($keys as $key => $valueKey) {
            if (is_object($objValue) && property_exists($objValue, $valueKey[0])) {
              $oneValueKey = $this->formatOptionValue($objValue->{$valueKey[0]}, @$valueKey[2]);
              if ($oneValueKey != $valueKey[1])
                $oneValue[$key] = $oneValueKey;
            } elseif (is_array($objValue) && array_key_exists($objValue, $valueKey[0])) {
              $oneValueKey = $this->formatOptionValue($objValue[$valueKey[0]], @$valueKey[2]);
              if ($oneValueKey != $valueKey[1])
                $oneValue[$key] = $oneValueKey;
            }
          }
          $newValue[] = $oneValue;
        }
        $value = $newValue;
        break;

      case 'bool':
        $value = (bool)$value;
        break;

      case 'int':
        $value = (int)$value;
        break;

      case '%':
        $value = min(max((int)$value, 0), 100) / 100;
        break;
    }

    return $value;
  }

  protected function getOptionValue($paramName, $valueDef = null, $valueType = null, $keys = [])
  {
    $value = $this->params->get($paramName, $valueDef);
    $value = $this->formatOptionValue($value, $valueType, $keys);

    return $value;
  }

  protected function requirePlugin($pluginName, $optionsDef = [])
  {
    $options = [];

    foreach($optionsDef as $def) {
      $value = $this->getOptionValue($def[1], $def[2], @$def[3], @$def[4]);

      if ($value && $value != $def[2])
        $options[$def[0]] = $value;
    }

    // Global object name
    $ga = $this->params->get('ga', 'ga');

    if (!empty($options))
      return "$ga('require', '" . $pluginName . "', " . json_encode($options) . ");\n";

    return "$ga('require', '" . $pluginName . "');\n";
  }

  public function onAfterDispatch()
  {    
    $dnt = JFactory::getApplication()->input->server->get('HTTP_DNT', 0);
    if (!$dnt && JFactory::getApplication()->isSite()) {
      $script = '';
      if ($this->params->get('tracking_id')) {
        $incAutotrack = false;
        $options = [];

        // Global object name
        $ga = $this->params->get('ga', 'ga');

        // User ID
        if ($this->params->get('user_id', 0)) {
          $user = JFactory::getUser();
          if (!$user->guest) {
            $options['userId'] = $user->id;
          }
        }

        // Linker
        if ($this->params->get('linker', 0)) {
          $options['allowLinker'] = true;
        }

        // Create tracker
        if ($ga != 'ga') {
          $script.= "window.GoogleAnalyticsObject = '$ga';\n";
        }
        $script.= "window.$ga=window.$ga||function(){($ga.q=$ga.q||[]).push(arguments)};$ga.l=+new Date;\n";
        $script.= "$ga('create', '" . $this->params->get('tracking_id') . "', 'auto'" . (empty($options) ? "" : ", " . json_encode($options)). ");\n";

        // Anonymize IP
        if ($this->params->get('anonymize_ip', 0)) {
          $script.= "$ga('set', 'anonymizeIp', true);\n";
        }

        // Display Features
        if ($this->params->get('df', 0)) {
          $options = [
            ['cookieName', 'df_cookie_name', '_gat'],
          ];
          $script.= $this->requirePlugin('displayfeatures', $options);
        }

        // Enhaced Link Attribution
        if ($this->params->get('ela', 0)) {
          $options = [
            ['cookieName', 'ela_cooie_name', '_ela'],
            ['duration', 'ela_duration', 30, 'int'],
            ['levels', 'ela_levels', 3, 'int'],
          ];
          $script.= $this->requirePlugin('linkid', $options);
        }

        // Linker
        if ($this->params->get('linker', 0)) {
          $script.= $this->requirePlugin('linker');
          $domains = $this->getOptionValue('linker_domains', [JFactory::getApplication()->input->server->get('HTTP_HOST')], 'array');
          $useAnchor = $this->getOptionValue('linker_use_anchor', false, 'bool');
          $decorateForms = $this->getOptionValue('linker_decorate_forms', false, 'bool');
          $script.= "$ga('linker:autoLink', " . json_encode($domains) . ", " . json_encode($useAnchor) . ", " . json_encode($decorateForms) . ");\n";
        }

        // cleanUrlTracker
        if ($this->params->get('cut', 0)) {
          $incAutotrack = true;
          $options = [
            ['stripQuery', 'cut_strip_query', false, 'bool'],
            ['queryParamsWhitelist', 'cut_query_params_whitelist', [], 'array'],
            ['queryDimensionIndex', 'cut_query_dimension_index', ''],
            ['indexFilename', 'cut_index_filename', ''],
            ['trailingSlash', 'cut_trailing_slash', ''],
          ];
          $script.= $this->requirePlugin('cleanUrlTracker', $options);
        }

        // eventTracker
        if ($this->params->get('et', 0)) {
          $incAutotrack = true;
          $options = [
            ['events', 'et_events', ['click'], 'array'],
            ['fieldsObj', 'et_fields_object', ['hitType' => 'event'], 'object'],
            ['attributePrefix', 'et_attribute_prefix', 'ga-'],
          ];
          $script.= $this->requirePlugin('eventTracker', $options);
        }

        // impressionTracker
        if ($this->params->get('it', 0)) {
          $incAutotrack = true;
          $options = [
            ['elements', 'it_elements', [], 'arrayobject', ['id' => ['id', ''], 'threshold' => ['threshold', 0, '%'], 'trackFirstImpressionOnly' => ['only_first_impression', true, 'bool']]],
            ['fieldsObj', 'it_fields_object', ['hitType' => 'event', 'eventCategory' => 'Viewport', 'eventAction' => 'impression'], 'object'],
            ['rootMargin', 'it_root_margin', ''],
            ['attributePrefix', 'it_attribute_prefix', 'ga-'],
          ];
          $script.= $this->requirePlugin('impressionTracker', $options);
        }

        // Pageview
        if ($this->params->get('clear_campaign', 0)) {
          $script.= "(function() {\n";
          $script.= "  var removeUtms = function() {\n";
          $script.= "    if (history.replaceState) {\n";
          $script.= "      if (window.location.search.indexOf('utm_') != -1\n";
          $script.= "      || window.location.hash.indexOf('utm_') != -1) \n";
          $script.= "        history.replaceState({}, '', window.location.toString().replace(/utm([^\&]+)\&?/gi, '').replace(/(\&|\?|#)$/, ''));\n";
          $script.= "    }\n";
          $script.= "  };\n";
          $script.= "  $ga('send', 'pageview', { 'hitCallback': removeUtms });\n";
          $script.= "})();\n";
        } else
          $script.= "$ga('send', 'pageview');\n";

        // include basic scripts
        JHtml::script('https://www.google-analytics.com/analytics.js', [], ['async' => 'async']);
        if ($incAutotrack) {
          if ($this->params->get('use_cdn', 1)) {
            JHtml::script('https://cdnjs.cloudflare.com/ajax/libs/autotrack/2.4.1/autotrack.js', [], ['async' => 'async']);
          } else {
            JHtml::script('plg_system_n3tautotrack/autotrack.js', ['version' => 'auto', 'relative' => true], ['async' => 'async']);
          }
        }

      } else {
        // If no tracking number
        if ($this->params->get('clear_campaign', 0)) {
          $script.= "(function() {\n";
          $script.= "  if (history.replaceState) {\n";
          $script.= "    if (window.location.search.indexOf('utm_') != -1\n";
          $script.= "    || window.location.hash.indexOf('utm_') != -1)\n";
          $script.= "      history.replaceState({}, '', window.location.toString().replace(/utm([^\&]+)\&?/gi, '').replace(/(\&|\?|#)$/, ''));\n";
          $script.= "  }\n";
          $script.= "})();\n";
        }
      }

      if ($script) {
        $script = ";\n" . $script;
        JFactory::getDocument()->addScriptDeclaration($script);
      }

      // UTM cookies
      if ($this->params->get('utm_cookies', 1)) {
        $utm_names = array(
          'utm_source',
          'utm_medium',
          'utm_campaign',
          'utm_term',
          'utm_content',
        );

        $input = JFactory::getApplication()->input;
        $is_utm = false;
        foreach($utm_names as $utm_name) {
          if ($input->get->get($utm_name)) {
            $is_utm = true;
            break;
          }
        }

        if ($is_utm) {
          if ($input->server->get('HTTP_REFERER'))
            $input->cookie->set('utm_referer', $input->server->get('HTTP_REFERER', null, 'url'), time() + 24 * 60 * 60 * $this->params->get('utm_cookies_lifetime', 120));
          else
            $input->cookie->set('utm_referer', null, time() - 1);
          foreach($utm_names as $utm_name) {
            if ($input->get->get($utm_name))
              $input->cookie->set($utm_name, $input->get->get($utm_name), time() + 24 * 60 * 60 * $this->params->get('utm_cookies_lifetime', 120));
            else
              $input->cookie->set($utm_name, null, time() - 1);
          }
        }
      }
    }
  }

}
