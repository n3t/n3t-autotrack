/**
 * @package n3t Autotrack
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2018-2019 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

// Global object
n3tAutotrack = window.n3tAutotrack || {};

// cleanUrlTracker
n3tAutotrack.cleanUrlTracker = n3tAutotrack.cleanUrlTracker || {};

n3tAutotrack.cleanUrlTracker.hitFilter = n3tAutotrack.cleanUrlTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// eventTracker
n3tAutotrack.eventTracker = n3tAutotrack.eventTracker || {};

n3tAutotrack.eventTracker.hitFilter = n3tAutotrack.eventTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// impressionTracker
n3tAutotrack.impressionTracker = n3tAutotrack.impressionTracker || {};

n3tAutotrack.impressionTracker.hitFilter = n3tAutotrack.impressionTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// maxScrollTracker
n3tAutotrack.maxScrollTracker = n3tAutotrack.maxScrollTracker || {};

n3tAutotrack.maxScrollTracker.hitFilter = n3tAutotrack.maxScrollTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// mediaQueryTracker
n3tAutotrack.mediaQueryTracker = n3tAutotrack.mediaQueryTracker || {};

n3tAutotrack.mediaQueryTracker.changeTemplate = n3tAutotrack.mediaQueryTracker.changeTemplate ||
  function(newValue, oldValue) {
    return oldValue + ' => ' + newValue;
  };

n3tAutotrack.mediaQueryTracker.hitFilter = n3tAutotrack.mediaQueryTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// outboundFormTracker
n3tAutotrack.outboundFormTracker = n3tAutotrack.outboundFormTracker || {};

n3tAutotrack.outboundFormTracker.shouldTrackOutboundForm = n3tAutotrack.outboundFormTracker.shouldTrackOutboundForm ||
  function (form, parseUrl) {
    var url = parseUrl(form.action);
    return url.hostname != location.hostname &&
      url.protocol.slice(0, 4) == 'http';
  };

n3tAutotrack.outboundFormTracker.hitFilter = n3tAutotrack.outboundFormTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// outboundLinkTracker
n3tAutotrack.outboundLinkTracker = n3tAutotrack.outboundLinkTracker || {};

n3tAutotrack.outboundLinkTracker.shouldTrackOutboundLink = n3tAutotrack.outboundLinkTracker.shouldTrackOutboundLink ||
  function(link, parseUrl) {
    var href = link.getAttribute('href') || link.getAttribute('xlink:href');
    var url = parseUrl(href);
    return url.hostname != location.hostname &&
      url.protocol.slice(0, 4) == 'http';
  };

n3tAutotrack.outboundLinkTracker.hitFilter = n3tAutotrack.outboundLinkTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// pageVisibilityTracker
n3tAutotrack.pageVisibilityTracker = n3tAutotrack.pageVisibilityTracker || {};

n3tAutotrack.pageVisibilityTracker.hitFilter = n3tAutotrack.pageVisibilityTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// socialWidgetTracker
n3tAutotrack.socialWidgetTracker = n3tAutotrack.socialWidgetTracker || {};

n3tAutotrack.socialWidgetTracker.hitFilter = n3tAutotrack.socialWidgetTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };

// urlChangeTracker
n3tAutotrack.urlChangeTracker = n3tAutotrack.urlChangeTracker || {};

n3tAutotrack.urlChangeTracker.shouldTrackUrlChange = n3tAutotrack.urlChangeTracker.shouldTrackUrlChange ||
  function(newPath, oldPath) {
    return newPath && oldPath;
  };

n3tAutotrack.urlChangeTracker.hitFilter = n3tAutotrack.urlChangeTracker.hitFilter ||
  function(fieldsObj, parseUrl) {
    return fieldsObj;
  };
