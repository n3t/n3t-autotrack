n3t Autotrack
=============

n3t Autotrack integrates Google Analytics [Autotrack][Autotrack] into Joomla!.

[Autotrack][Autotrack] is a JavaScript library built on top of analytics.js that makes
it easier for web developers to track the user interactions that are common to most websites.

[Autotrack]: https://github.com/googleanalytics/autotrack/