Release notes
=============

3.0.x
-----

#### 3.0.3

 - Better clear campaigns parameters code


#### 3.0.2

 - Clear campaigns parameters option
 - Keep campaigns parameters in cookies option

#### 3.0.1

 - Autotrack script is loaded only when needed

#### 3.0.0

 - Initial release