<?php
/**
 * @package n3t Autotrack
 * @author Pavel Poles - n3t.cz
 * @copyright (C) 2018-2019 - Pavel Poles - n3t.cz
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

use Joomla\Registry\Registry;

class plgSystemN3tAutotrackInstallerScript
{
  var $oldVersion = null;
  var $newVersion = null;


  public function preflight($type, $parent)
  {
    if ($type == 'update') {
      $eid = $parent->get('currentExtensionId');

      $db = JFactory::getDbo();
      $query = $db->getQuery(true)
        ->select($db->quoteName('manifest_cache'))
        ->from($db->quoteName('#__extensions'))
        ->where($db->quoteName('extension_id') . ' = ' . $db->quote($eid));
      $db->setQuery($query);
      $manifest = $db->loadResult();

      $manifest = new Registry($db->loadResult());
      $this->oldVersion = $manifest->get('version');
    }
  }

	public function update($parent)
	{
    $this->newVersion = $parent->get('manifest')->version;
    JFactory::getApplication()->enqueueMessage(JText::sprintf(strtoupper($parent->get('name')) . '_INSTALL_UPDATE_VERSION', $this->oldVersion, $this->newVersion));
  	$this->checkUpdateServers($parent);
	}

  protected function checkUpdateServers($parent)
	{
    $eid = $parent->get('currentExtensionId');
		$manifest = $parent->getManifest();
    
		$updateservers	= $manifest->updateservers;
		if ($updateservers) 
      $updateservers = $updateservers->children();
		else
			$updateservers = array();
      
    $locations = array();
		foreach ($updateservers as $updateserver)
      $locations[] = trim($updateserver); 
      
    $db = JFactory::getDbo();
    
    $query = $db->getQuery(true)
      ->select($db->quoteName(array('a.update_site_id', 'a.location')))      
      ->from($db->quoteName('#__update_sites', 'a'))
      ->join('INNER', $db->quoteName('#__update_sites_extensions', 'b') . ' ON (' . $db->quoteName('a.update_site_id') . ' = ' . $db->quoteName('b.update_site_id') . ')')
      ->group($db->quoteName(array('a.update_site_id', 'a.location')))            
      ->where($db->quoteName('b.extension_id') . '=' . $eid);
    $current_sites = $db->setQuery($query)->loadObjectList();

    $delete_sites_extensions = array();
    $delete_sites = array();
    foreach($current_sites as $current_site) {
      if (!in_array($current_site->location, $locations)) {
        $delete_sites_extensions[] = $current_site->update_site_id;
        $query->clear()
          ->select('count('.$db->quoteName('extension_id').')')        
          ->from($db->quoteName('#__update_sites_extensions'))
          ->where($db->quoteName('update_site_id') . '=' . $current_site->update_site_id)
          ->where($db->quoteName('extension_id') . '!=' . $eid);
        $current_site->extensions_count = $db->setQuery($query)->loadResult();
         
        if ($current_site->extensions_count == 0)
          $delete_sites[] = $current_site->update_site_id;
      }
    }    
    
    if (count($delete_sites_extensions)) {
      $query->clear()
        ->delete($db->quoteName('#__update_sites_extensions'))
        ->where(array(
          $db->quoteName('extension_id') . '=' . $eid,
          $db->quoteName('update_site_id') . ' in (' . implode(',', $delete_sites_extensions) . ')'          
        ));        
      $db->setQuery($query)->execute();
    }  
    
    if (count($delete_sites)) {
      $query->clear()
        ->delete($db->quoteName('#__update_sites'))
        ->where($db->quoteName('update_site_id') . ' in (' . implode(',', $delete_sites) . ')');
      $db->setQuery($query)->execute();
    }
	}
  
}